module.exports = {
	options: {
		srcPrefix: 'bower_components'
	},
	scripts: {
		options: {
			destPrefix: 'src/scripts/vendor'
		},
		files: {
			'jquery/jquery.js': 'jquery/dist/jquery.js',
			'modernizr/modernizr.js': 'modernizr/modernizr.js',
			'respond/respond.js': 'respond/dest/respond.min.js',
			'requirejs/require.js': 'requirejs/require.js',
			'unslider/unslider.js': 'unslider/src/unslider.js',
			'owlcarousel/owl.carousel.js': 'OwlCarousel/owl-carousel/owl.carousel.js'
		}
	}
};
