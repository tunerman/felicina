module.exports = {
  html: {
    files: [{
      expand: true,
      flatten: true,
      src: ['src/html/**/*.html'],
      dest: 'dist/'
    }]
  },

  fonts: {
    files: [{
      expand: true,
      flatten: false,
      cwd: 'src/fonts',
      src: ['**/*.{eot,svg,ttf,woff,woff2}'],
      dest: 'dist/assets/static/fonts'
    }]
  },

  svg: {
    files: [{
      expand: true,
      flatten: false,
      cwd: 'src/images/svg',
      src: ['**/*.svg'],
      dest: 'dist/assets/static/images/svg'
    }]
  },

  json: {
    files: [{
      expand: true,
      flatten: false,
      cwd: 'src/json',
      src: ['**/*.json'],
      dest: 'dist/assets/static/json'
    }]
  },

  ico: {
    files: [{
      expand: true,
      flatten: false,
      cwd: 'src/favicon',
      src: ['**/*.ico'],
      dest: 'dist/assets/static/favicon'
    }]
  }

};
