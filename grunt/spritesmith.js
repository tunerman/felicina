module.exports = {
    all: {
      src: 'src/images/sprites/*.png',
      dest: 'dist/assets/static/images/sprites/sprites.png',
      destCss: 'src/styles/bootstrap/sprites.less',
      cssFormat: 'less'
    }
};
