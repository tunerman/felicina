module.exports = {

	// Настройки задач
	options: {
		limit: 3
	},

	// Задачи для разработки
	devFirst: [
		'clean',
		'jshint'
    //'spritesmith'
	],
	devSecond: [
		'less:dev',
		'uglify'
	],
  devThird: [
    'copy:html',
    'copy:fonts',
    'copy:svg',
    'copy:json'
  ],

	// Задачи для продакшна
	prodFirst: [
		'clean',
		'jshint'
    //'spritesmith'
],
	prodSecond: [
		'less:prod',
		'uglify:prod'
	],
  prodThird: [
    'copy:html',
    'copy:fonts',
    'copy:svg',
    'copy:json'
  ],

	// Image tasks
	imgFirst: [
		'imagemin',
    //'sprite'
	]
};
