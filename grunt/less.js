module.exports = {
	// Настройки для разработки
	dev: {
		options: {
      sourceMap: true,
      sourceMapURL: '/assets/static/styles/main.css.map'
		},
		files: [{
			expand: true,
			cwd: 'src/styles',
			src: ['*.less'],
			dest: 'dist/assets/static/styles',
			ext: '.css'
		}]
	},
	// Настройки для продакшна
	prod: {
		options: {
      sourceMap: false,
      compress: true
		},
		files: [{
			expand: true,
			cwd: 'src/styles',
			src: ['*.less'],
			dest: 'dist/assets/static/styles',
			ext: '.css'
		}]
	}
};
