module.exports = {

	all: {
    options: {
      beautify: true,
      sourceMap: true,
      sourceMapName: '/assets/static/scripts/main.js.map'
    },
		files: [{
			expand: true,
			cwd: 'src/scripts',
			src: '**/*.js',
			dest: 'dist/assets/static/scripts',
			ext: '.min.js',
      extDot: 'first'
		}]
	},
  prod: {
    options: {
      beautify: false,
      sourceMap: false,
      compress: {}
    },
    files: [{
      expand: true,
      cwd: 'src/scripts',
      src: '**/*.js',
      dest: 'dist/assets/static/scripts',
      ext: '.min.js',
      extDot: 'first'
    }]
  }
};
