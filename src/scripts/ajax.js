var Ajax = function () {

  return {
    initExhibitsPortlet: function(externalParam) {

      var defaultParam = {
        method: "GET"
      };
      var param = $.extend({}, defaultParam, externalParam);

      $(".j_get_exhibits").on("click", function(e) {
        var $this = $(this);
        var formId = $this.data("form");
        var $form = $(formId);

        var $request = $.ajax({
          type: param.method,
          url: param.url,
          data: $form.serializeArray(),
          dataType: "json"
        });

        $request.success(function(data, textStatus, jqXHR) {
          var target = $this.data("target");
          var $container = $(target);
          var html = '';

          $.each(data, function(i, item) {
            html+= '<a href="'+ item.image +'" class="js__modal" rel="exhibits__1" title="'+ item.title +'"><img src="'+item.thumb+'" class="img-responsive" alt=""/></a>';
          });
          $container.trigger("add", html);

        });

        $request.error(function(jqXHR, textStatus, errorThrown) {
          console.log("Error: "+textStatus);
        });

        e.preventDefault();
      });

    }
  };

}();
