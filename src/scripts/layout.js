var Layout = function () {

  // IE mode
  var isRTL = false;
  var isIE8 = false;
  var isIE9 = false;
  var isIE10 = false;
  var isIE11 = false;

  var handleInit = function() {

    isIE8 = !! navigator.userAgent.match(/MSIE 8.0/);
    isIE9 = !! navigator.userAgent.match(/MSIE 9.0/);
    isIE10 = !! navigator.userAgent.match(/MSIE 10.0/);
    isIE11 = !! navigator.userAgent.match(/MSIE 11.0/);

  };


  /**
   * Фиксы для старых версий IE
   */
  var handleIEFixes = function() {
    //fix html5 placeholder attribute for ie7 & ie8
    if (isIE8 || isIE9) { // ie8 & ie9
      // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
      jQuery('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function () {

        var input = jQuery(this);

        if (input.val() === '' && input.attr("placeholder") !== '') {
          input.addClass("placeholder").val(input.attr('placeholder'));
        }

        input.focus(function () {
          if (input.val() == input.attr('placeholder')) {
            input.val('');
          }
        });

        input.blur(function () {
          if (input.val() === '' || input.val() == input.attr('placeholder')) {
            input.val(input.attr('placeholder'));
          }
        });
      });
    }
  };


  /**
   * Обработчик по поиску
   */
  var handleSearch = function() {

  };


  /**
   * Обработчик аккордионов
   */
  var handleAccordion = function() {

    var accordion_list = $('.j__accordion > .accordion__body');
    $('.j__accordion .accordion__heading__title > a').click(function(e) {
      var $accordion = $(this).parent().parent().parent();
      var active = $accordion.hasClass("accordion--active");
      if (active) {
        $accordion.removeClass("accordion--active");

      } else {
        $accordion.addClass("accordion--active");
      }
      e.preventDefault();
    });
  };


  /**
   * Открытие\закртыие сайдбара в мобильных устройствах
   */
  var handleSidebarMenu = function () {
    $(".j_mobitoggler").on("click", function() {
      var $wrapper = $(".wrapper");

      if ($wrapper.hasClass("toggled")) {
        $wrapper.removeClass("toggled");
      } else {
        $wrapper.addClass("toggled");
      }
    });
  };


  /**
   * Инициализация плагина FancyBox для модальных окон
   */
  var handleFancybox = function () {
    if (!jQuery.fancybox) {
      return;
    }

    if (jQuery(".js__modal").size() > 0) {

      jQuery(".js__modal").fancybox({
        maxWidth    : 800,
        maxHeight   : 600,
        fitToView   : false,
        width       : '70%',
        height      : '70%',
        autoSize    : false,
        openEffect  : 'none',
        closeEffect : 'none',
        tpl: {
          next: '<a title="Следующая" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
          prev: '<a title="Предыдущая" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
        },
        helpers: {
          title: {
            type: 'inside'
          }
        }
      });

      jQuery('.js__modal__ajax').fancybox({
        type: 'iframe'
      });
    }
  };

  /**
   * Инициализация стика
   */
  var handleSticky = function () {

    var $sticky  = jQuery(".j__stick");

    if ($sticky.size() > 0) {

      $sticky.sticky({
        topSpacing: 0
      });
    }
  };

  var handleScrollbar = function() {

    var $scrollbar = jQuery(".j__scrollbar");

    if ($scrollbar.size() > 0) {
      $scrollbar.perfectScrollbar();
    }
  };

  return {
    init: function () {
      handleInit();
      handleIEFixes();
      handleSearch();
      handleFancybox();
      handleSticky();
      handleScrollbar();
      handleSidebarMenu();
      handleAccordion();
    },

    /**
     * Инициализация плагина Owl Carousel
     */
    initOWL: function () {

      var _owlNavigation = function(owl, owlNav, minCount) {

        if (owl.find(".owl-item").length > minCount) {
          owlNav.removeClass("invisible");
        }
        owlNav.children("a").on("click", function(e) {
          var fn = this.className.split(' ')[1];
          owl.data('owlCarousel')[fn]();
          e.preventDefault();
        });
      };

      // Инициализация owl слайдера на главной
      $(".owl-slider").owlCarousel({
        singleItem : true,
        pagination: true,
        scrollPerPage : true,
        slideSpeed : 600,
        paginationSpeed : 400,
        rewindSpeed : 600,
        responsiveRefreshRate : 200,
        afterInit: function() {
          _owlNavigation($(".owl-slider"), $(".j_slider__nav"), 1);
        }
      });

      // Инициализация owl портлета для колекций
      $(".owl-carousel-collection-portlet").owlCarousel({
        singleItem : true,
        afterInit: function() {
          _owlNavigation($(".owl-carousel-collection-portlet"), $(".j_collection__nav"), 1);
        }
      });

      // Инициализация owl портлета для экспозиций
      $(".owl-carousel-exposition-portlet").owlCarousel({
        items: 2,
        responsive: false,
        afterInit: function() {
          _owlNavigation($(".owl-carousel-exposition-portlet"), $(".j_exposition__nav"), 2);
        }
      });

      // Инициализация owl для подробного описания экспозиции
      $(".owl-carousel-exposition-view").owlCarousel({
        items: 4,
        navigation: true
      });
    },

    /**
     * Инициализация карты для контактов
     */
    initContactMap: function(param) {

      var json = $.parseJSON(param);

      ymaps.ready().done(function(ym) {

        var map =  new ym.Map("j__contacts__map", {
          center: [45.025285,38.971783],
          zoom: 17,
          controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
        });

        var collection = new ym.GeoObjectCollection();
        var affiliates = [];

        $.each(json.affiliates, function(i, model) {

          collection.add(affiliates[model.id] = new ym.Placemark([model.coordinates[0], model.coordinates[1]], {
            balloonContent: "<div class='row'><div class='col-md-12'>"+model.properties.balloonContent+"</div></div>",
            balloonContentHeader: model.properties.balloonContentHeader
          },{
            iconLayout: 'default#image',
            iconImageHref: "/assets/static/images/content/structure/tags/default.png",
            iconImageSize: [30, 38],
            iconImageOffset: [-3, -38]
          }));
        });

        map.geoObjects.add(collection);
      });

    },

    /**
     * Инициализация карты для структуры
     */
    initStructureMap: function(param) {

      var json = $.parseJSON(param);

      ymaps.ready().done(function(ym) {
        var map =  new ym.Map("j__structure__map", {
          center: [45.025308, 38.974231],
          zoom: 7,
          controls: ['zoomControl', 'typeSelector',  'fullscreenControl']
        });

        var collection = new ym.GeoObjectCollection();
        var affiliates = [];


        $.each(json.affiliates, function(i, model) {

          collection.add(affiliates[model.id] = new ym.Placemark([model.coordinates[0], model.coordinates[1]], {
            balloonContent: "<div class='row'><div class='col-md-4'><img src="+model.properties.balloonImageContent+" alt='' /></div><div class='col-md-8'>"+model.properties.balloonContent+"</div></div>",
            balloonContentHeader: model.properties.balloonContentHeader,
            balloonContentFooter: "<a href='"+model.properties.balloonContentFooter+"'>Читать подробнее</a>"
          },{
            iconLayout: 'default#image',
            iconImageHref: "/assets/static/images/content/structure/tags/default.png",
            iconImageSize: [30, 38],
            iconImageOffset: [-3, -38]
          }));
        });

        $(".j__structure__open_balloon").on("click", function (e) {

          var $this = $(this);
          var id = $this.data("id");

          affiliates[id].balloon.open();

          $(".structure__item").each(function() {
            $(this).removeClass("structure__item__active");
          });

          $this.parentsUntil(".structure__item").last().parent().addClass("structure__item__active");
          e.preventDefault();
        });

        map.events.add(['balloonclose'], function (e) {
          $(".structure__item").each(function() {
            var $this = $(this);
            $this.removeClass("structure__item__active");
          });
        });

        map.geoObjects.add(collection);
        map.setBounds(collection.getBounds());

      });

    },

    /**
     * Вертикальный слайдер
     */
    initStructureBXslider: function() {
      $(document).ready(function() {
        var list = $(".js__structure_slider");
        var listWrapper = $(".js__structure_slider__wrapper");
        if (list.find(".structure__item").size() > 3) {
          var slider = list.bxSlider({
            mode: "vertical",
            pager: false,
            minSlides: 3,
            maxSlides: 3,
            slideWidth: 305,
            autoStart: true,
            moveSlides: 1,
            controls: false,
            infiniteLoop: false,
            onSliderLoad: function() {
              listWrapper.removeClass("structure_slider__load");
              $(".js__structure__slider__controls").show();
            }
          });
        }

        $(".js__structure_slider__prev").on("click", function(e) {
          slider.goToPrevSlide();
          e.preventDefault();
        });

        $(".js__structure_slider__next").on("click", function(e) {
          slider.goToNextSlide();
          e.preventDefault();
        });
      });

    },

    /**
     * Инициализация плагина jQuery Montage, чтобы формировать посты-мозайки
     */
    initMontage: function () {

      var $imageTiles	= $('.js__image__tiles');
      if ($imageTiles.length > 0) {

        $imageTiles.each(function() {
          var $imageTile = $(this);
          var $imagesInTile	= $imageTile.find('img').hide();
          var paramAlternateHeight = typeof $imageTile.data("alternate-height") !== 'undefined' ? $imageTile.data("alternate-height") : 1;
          var paramAlternateHeightMin = typeof $imageTile.data("alternate-height-min") !== 'undefined' ? $imageTile.data("alternate-height-min") : 100;
          var paramAlternateHeightMax = typeof $imageTile.data("alternate-height-max") !== 'undefined' ? $imageTile.data("alternate-height-max") : 150;
          var paramMinw = typeof $imageTile.data("minw") !== 'undefined' ? $imageTile.data("minw") : 150;
          var paramMargin = typeof $imageTile.data("margin") !== 'undefined' ? $imageTile.data("margin") : 3;
          var paramAjax = typeof $imageTile.data("ajax") !== 'undefined' ? $imageTile.data("ajax") : 0;
          var paramAjaxHandler = $imageTile.data("ajax-handler");
          var paramEmptyContainer = typeof $imageTile.data("empty-container") !== 'undefined' ? $imageTile.data("empty-container") : 0;
          var counter = 0;

          $imagesInTile.each(function() {
            var $imageInTile = $(this);

            $('<img/>').load(function() {
              ++counter;
              if( counter === $imageInTile.length ) {
                $imagesInTile.show();
                $imageTile.montage({
                  margin: paramMargin,
                  minw : paramMinw,
                  minh: 30,
                  alternateHeight: paramAlternateHeight,
                  alternateHeightRange: {
                    min : paramAlternateHeightMin,
                    max : paramAlternateHeightMax
                  },
                  fillLastRow: true
                });

                if (paramAjax) {
                  $(paramAjaxHandler).show().on("add", function(e, images) {
                    if (paramEmptyContainer) {
                      var $images = $(images);
                      $(paramAjaxHandler).empty();
                      $images.imagesLoaded( function(){
                        $imageTile.append( $images ).montage( 'add', $images );
                      });
                    }
                  });
                }
              }
            }).attr('src',$imageInTile.attr('src'));
          });
        });


      }
    },

    // wrapper function to scroll(focus) to an element
    scrollTo: function (el, offeset) {
      var pos = (el && el.size() > 0) ? el.offset().top : 0;
      if (el) {
        if ($('body').hasClass('page-header-fixed')) {
          pos = pos - $('.header').height();
        }
        pos = pos + (offeset ? offeset : -1 * el.height());
      }

      jQuery('html,body').animate({
        scrollTop: pos
      }, 'slow');
    },

    scrollTop: function () {
      App.scrollTo();
    }

  };
}();
